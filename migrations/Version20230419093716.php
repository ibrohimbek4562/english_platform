<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230419093716 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE purchases (id INT AUTO_INCREMENT NOT NULL, payed_amound INT NOT NULL, is_payed TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchases_user (purchases_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_7413E035559939B3 (purchases_id), INDEX IDX_7413E035A76ED395 (user_id), PRIMARY KEY(purchases_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchases_category (purchases_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_F8A14D34559939B3 (purchases_id), INDEX IDX_F8A14D3412469DE2 (category_id), PRIMARY KEY(purchases_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE purchases_user ADD CONSTRAINT FK_7413E035559939B3 FOREIGN KEY (purchases_id) REFERENCES purchases (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchases_user ADD CONSTRAINT FK_7413E035A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchases_category ADD CONSTRAINT FK_F8A14D34559939B3 FOREIGN KEY (purchases_id) REFERENCES purchases (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchases_category ADD CONSTRAINT FK_F8A14D3412469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE purchases_user DROP FOREIGN KEY FK_7413E035559939B3');
        $this->addSql('ALTER TABLE purchases_user DROP FOREIGN KEY FK_7413E035A76ED395');
        $this->addSql('ALTER TABLE purchases_category DROP FOREIGN KEY FK_F8A14D34559939B3');
        $this->addSql('ALTER TABLE purchases_category DROP FOREIGN KEY FK_F8A14D3412469DE2');
        $this->addSql('DROP TABLE purchases');
        $this->addSql('DROP TABLE purchases_user');
        $this->addSql('DROP TABLE purchases_category');
    }
}
